import java.io.PrintStream;

public class BstMultiset<T> extends Multiset<T>
{
	/*Reference to the root of the tree*/
	protected Node root;
	
	/*Reference to the left child of the tree*/
	protected Node leftChild;
	
	/*Reference to the right child of the tree*/
	protected Node rightChild;

	/* constructor */
	public BstMultiset() {
		root = null;
	} 
	
	/* add new items */
	public void add(T item) {
		// Implement me!
		
		/* declare and instantiate newNode variable */
		Node newNode = new Node((String)item);
		
		if (root == null)
		{
			root = newNode;
			System.out.println("root set");
			return;
		}
	
		/* instantiate leafNode */
		Node leafNode = getLeafNode(newNode);
	
		/* if item is already contained in BST , increment ammount */
		if (newNode.getValue().equals(leafNode.getValue())) 
		{
			leafNode.addCount();
			return;   
		}
			
		/* compare newNode with available nodes , and traverse tree accordingly */
		if ( compare(newNode ,  leafNode )) /* go right */
		{
			leafNode.setRightChild(newNode);
		}
		else /* if its less than -> go left */
		{
			leafNode.setLeftChild(newNode);
		}
		
	} // end of add()
	
	/*function for comparing nodes*/
	public Boolean compare(Node newNode, Node currNode) {
		/* if newNode is > than currNode , positive value will be returned*/
		int comparisonValue = newNode.getValue().compareToIgnoreCase(currNode.getValue());

		if(comparisonValue > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	} //end of compare() method

	/* search BST */
	public int search(T item) {
		/* search for item node */
		Node tempNode = compareAndSearch(new Node((String)item));
		if(tempNode != null)
		{
			//return the number of items
			return tempNode.getCount();	
		}
		// default return, please override when you implement this method
		return 0;
	} // end of add()
	
	/* compare and search through BST */
	public Node compareAndSearch(Node searchNode) {
		/* instantiate node */		
		Node currNode = root;
		int i = 0;
			
		//Node parentNode = getParentNode(searchNode);
		while(currNode !=  null)
		{			
			if (currNode.getValue().equals(searchNode.getValue()))
			{
				return currNode;
			}
				
			//System.out.println("value of searchNode = " +searchNode.getValue());
			if ( currNode.getLeftChild() == null && currNode.getRightChild() == null)
			{
				return null;
			}

			/* compare newNode with available nodes */
			if ( compare(searchNode ,  currNode )) /* go right */
			{
				//System.out.println("/* go right */");
				if ( currNode.getRightChild() != null)
				{
				//System.out.println("currNode.getRightChild = " + currNode.getRightChild().getValue());
					currNode = currNode.getRightChild();	
				}
				else
				{
					break;
				}
			}
			else if ( !compare(searchNode ,  currNode ))/* if its less than -> go left */
			{
				if ( currNode.getLeftChild() != null)
				{
					//	System.out.println("currNode.getLeftChild = " + currNode.getLeftChild().getValue());
					currNode = currNode.getLeftChild();
				}
				else
				{
					break;						
				}
			}				
		}
		return null;
	}

	/* get lead node of specific node */
	private Node getLeafNode( Node newNode ){
		
		/* instantiate node */
		Node currNode = root;
		
		while ( currNode != null)
		{
			if ( currNode.getValue().equals(newNode.getValue()))
			{
				break;
			}
			
			if ( compare(newNode ,  currNode )) /* go right */
			{
				if (currNode.getRightChild() != null)
				{
					currNode = currNode.getRightChild();
				}
				else
				{
					break;
				}	
			}
			else /* if its less than -> go left */
			{
				if (currNode.getLeftChild() != null)
				{
					currNode = currNode.getLeftChild();
				}
				else
				{	
					break;
				}
			}
		}
		
		return currNode;
	}
	
	/* remove one instance of item */
	public void removeOne(T item) {
		
		/* instantiate required nodes */
		Node removalNode = new Node((String) item);
		removalNode = compareAndSearch(removalNode);
		
		/* if removal item not avaialble in BST , return */
		if (removalNode == null) { return; }
		
		/* if more than one instance exists , decrement ammount of item*/
		if(removalNode.getCount() > 1)
		{
			removalNode.count--;
			return;
		}
		
		/* else remove last instance of item */
		removeAll(item);	
		
	} // end of removeOne()
	
	/* get parent node of particular node */
	public Node getParentNode(Node searchNode){
		/* instantiate node */
		Node currNode = root;

		/* if search node is the root , i.e no parents -> return root*/
		if(searchNode.getValue().equals(root.getValue()))
		{
			return root;
		}
		
		/* traverse through BST , searching for parent Node of the search Node */
		while(currNode !=null)
		{
			/* determine if left child is the search node , i.e currNode is the parent node*/
			if (currNode.getLeftChild() != null && currNode.getLeftChild().getValue().equals(searchNode.getValue()))
			{
				return currNode;
			}
			
			/* determine if right child is the search node , i.e currNode is the parent node*/
			if (currNode.getRightChild() != null && currNode.getRightChild().getValue().equals(searchNode.getValue()))
			{
				return currNode;
			}
		
		
			/* compare newNode with available nodes */
			if ( compare(searchNode ,  currNode)) /* go right */
			{
				currNode = currNode.getRightChild();		
			}
			else /* if its less than -> go left */
			{
				currNode = currNode.getLeftChild();		
			}
				
		}

			return null;
		}
	
	/* get child node of parent node */
	private Node getChildNode (Node nodeInFocus){
		
		/* if node has child on left */
		if ( nodeInFocus.getLeftChild() != null )
		return nodeInFocus.getLeftChild();
	
		/* else return child on right */
		return nodeInFocus.getRightChild();
		
	}
	
	/* remove all instances of particular item */
	public void removeAll (T item){
	
		/* search and retrieve the specified node to be removed , if not avaialble then return*/
		Node removalNode = compareAndSearch(new Node(((String) item)));
		if (removalNode == null ) { return; }
		
		/* get prerequisite data about the removal node , parentNode and number of children */
		Node parentNode = getParentNode(removalNode);
		int numberOfChildren = getNumberOfChildNodes(removalNode);
		
		/* different cases depending on the number of children the removal node has */
		switch( numberOfChildren ) {
			case 0 :
			
				/* if node to removed (removalNode) is the root ) */
				if ( removalNode == root )
				{
					root = null;
					return;
				}	
			
				compareAndRearrangeNodeProperties( removalNode , parentNode , null );
				break;
				
			case 1:
				
				/* instantaite Child Node */
				Node childNode = getChildNode(removalNode);
				
				if ( removalNode == root )
				{
					compareAndRearrangeNodeProperties( removalNode , root , null );
					root = removalNode;
					
				}
				else
				{
					compareAndRearrangeNodeProperties( removalNode , parentNode , childNode );	
				}
				break;
			case 2:
				
				/* in case of 2 children , get the nextLargestNode avaialble and its parent. */
				Node nextLargestNode = getNextLargestNode(removalNode);
				Node parentOfNextLargestNode = getParentNode(nextLargestNode);
				
				/* if the root is been removed , restructure the tree accordingly */
				if ( removalNode == root )
				{
					
					if ( checkChildren(nextLargestNode))
					{
						Node childOfNextLargestNode = getChildNode(nextLargestNode);
						compareAndRearrangeNodeProperties( childOfNextLargestNode , parentOfNextLargestNode , childOfNextLargestNode );
					}
				
					removalNode.setValue( nextLargestNode.getValue());
					root = removalNode;		
					return;
				}
				
				/* check children for the nextLargestNode , and reset the properties of nodes affected appropiately. */
				if ( checkChildren(nextLargestNode))
				{
					Node childOfNextLargestNode = getChildNode(nextLargestNode);
					compareAndRearrangeNodeProperties( childOfNextLargestNode , parentOfNextLargestNode , childOfNextLargestNode );
				}
				else
				{
					compareAndRearrangeNodeProperties( removalNode , parentOfNextLargestNode , null );
				}
				
				removalNode.setValue( nextLargestNode.getValue());
				break;
		}
		
	}
	
	/* compare between different nodes , and set properties accordingly */
	private void compareAndRearrangeNodeProperties( Node removalNode , Node parentNode , Node setNode) {
		if ( compare(removalNode , parentNode))
		{
			parentNode.setRightChild(setNode);
		}
		else 
		{
			parentNode.setLeftChild(setNode);
		}
	}
	
	/* method for finding next largest node in subtree */
	public Node getNextLargestNode(Node nodeInfocus) {
		
		/* value on right child should always be larger , so check right side of node */
		if (nodeInfocus.getRightChild() != null)
		{
			/* if this condiiton is met , the first right child is the next largest available node , so return it */
			if(nodeInfocus.getRightChild().getLeftChild() == null)
			{	
				return nodeInfocus.getRightChild();
			}
			else /* else travese through list , following a pattern that will determine the next largest node */
			{
				Node currentNode = nodeInfocus.getRightChild();
				while (currentNode.getLeftChild() != null)
				{	

					currentNode = currentNode.getLeftChild();

				}
			
			/* return the corresponding node */
			return currentNode;
			}				

		}
		
		/* if no larger value exists , return null */
		return null;
	}
	
	/* check number of child nodes a node has */
	private int getNumberOfChildNodes( Node nodeInFocus ) {
		
		boolean leftLeafChild = nodeInFocus.getLeftChild() == null ;
		boolean rightLeafChild = nodeInFocus.getRightChild() == null;


		if ( leftLeafChild && rightLeafChild ) { return 0; } 		/* if number of childer is 0 , return 0 */
		else if ( !leftLeafChild && !rightLeafChild ) { return 2; } /* if number of childer is 2 , return 2 */
		else { return 1; }											/* if number of childer is 1 , return 1 */
		
	}
	
	/* function that checks if nodes have children or not */
	public boolean checkChildren( Node nodeInfocus) {
		
		if (nodeInfocus.getRightChild() != null)
		{
			//System.out.println("node has children");
			return true;
		}
		
		return false;
	}
	
	/* print BST */
	public void print(PrintStream out) {
		// Implement me!
		Node currNode = root;
	
		printInOrderRec(root,out);
		
	} // end of print()
	
	/* print the BST in order using recursive functions */
	private void printInOrderRec(Node currNode, PrintStream out){
		/* if the currNode is equal to null , terminate */
		if ( currNode == null ){ return; }
	
		/* recursively traverse through BST */
		printInOrderRec(currNode.getLeftChild(),out);
		
		out.printf("%s | %d\n", currNode.getValue(),currNode.getCount());
		
		printInOrderRec(currNode.getRightChild(),out);
	
	}

	/* node class */
	private class Node{
		
		private String mValue;
		private Node leftChild;
		private Node rightChild;
		int count;
		
		public Node(String value)
		{
			mValue = value;
			leftChild = null;
			rightChild = null;
			count = 1;
			
		}
		
		public void addCount() {
            count++;
        }

        public void minusCount() {
            count--;
        }
		
		public int getCount(){
			return count;
		}
		
		public String getValue(){
			return mValue;
		}
		
		public Node getLeftChild(){
			return leftChild;
		}
		
		public Node getRightChild(){
			return rightChild;
		}
		
		public void setValue(String value){
			mValue = value;
		}
		
		public void setRightChild(Node right){
			rightChild = right;
		}
		
		public void setLeftChild(Node left){
			leftChild = left;
		}
	}
} // end of class BstMultiset