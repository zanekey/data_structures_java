import java.io.PrintStream;


public class LinkedListMultiset<T> extends Multiset<T>
{
	/*Reference to the head of the list*/
	protected Node head;
	
	/*Reference to the tail of the list*/
	protected Node tail;
	
	/*The length of the list*/
	protected int length;
	
	/*Constructor */
	public LinkedListMultiset() {	
		head = null;
		tail = null;
		length = 0;		
	} // end of LinkedListMultiset()
	
	/* add node to list */
	public void add(T item) {
		
		/* instatiate nodes */
		Node newNode = new Node((String) item);
		Node currNode = head;
		
		/* if item is first item to be added to the list */
		if(head == null)
		{
			head = newNode;
			tail = newNode;
			length++;
			return;
		}
		
		//traverse through the list
		while(currNode != null)
		{
			/* check if item already exists in list , if so increment item count */
            if(currNode.getValue().equals(newNode.getValue())) 
			{
                currNode.addCount();
				return;
            }
	
			currNode = currNode.getNext();
		}
			
		/* else add item to the tail of list */
		tail.setNext(newNode);
		newNode.setPrev(tail);
		tail = newNode;
		
		/* increment length of list */
		length++;
		
	} // end of add()
	
	/* search for item */
	public int search(T item) {
			// Implement me!		
			Node currNode = head;
			
			// traverse through list , search for item
			while(currNode != null)
			{
				
				if(currNode.getValue().equals((String) item))
				{
					/* return count of item */
					return currNode.getCount();
				}
				
				/* traverse to the next node */
				currNode = currNode.getNext();		
			}
			
			/* if item not avaialble in list , return 0 */
			return 0;
		}

	/* get index of item */
	public int getItemIndex(T item) {
		
		/* isntantite variables */
		Node currNode = head;
		/* index of item */
		int index = 0;
		
		//traverse through the list, search for item
		while(currNode != null)
		{
			if(currNode.getValue().equals((String) item))
			{
				//return the index of item
				return index;
			}
			
			// traverse to next node
			currNode = currNode.getNext();
			// increment index
			index++;
		}
		
		/* else return */
		return index;
	}

	/* remove one instance of item */
	public void removeOne(T item) {
			
			/* check item exists in list , if it doesn't -> return */
			if  (search(item) == 0) { return; }
			
			/* instantiate traversal node */
			Node currNode = head;
			
			/* get index of item in linked list */
			int index = getItemIndex(item);

			/* traverse to index position */
			for ( int i = 0; i < index ; i++)
			{
				currNode = currNode.getNext();
			}

			/* check items count */
			if ( currNode.getCount() == 1)
			{
				removeNode(currNode);
				return;
			}
			
			/* if more than one item exists , decrement ammount */
			currNode.count--;
			
		} // end of removeOne()
		
	/* remove all occurrence of item */
	public void removeAll(T item) {
		
		/* check item exists in list , if it doesn't -> return */
		if  (search(item) == 0) { return; }
		
		/* instantiate traversal node */
		Node currNode = head;
		
		/* get index of item in linked list */
		int index = getItemIndex(item);

		/* traverse to index position */
		for ( int i = 0; i < index ; i++)
		{
			currNode = currNode.getNext();
		}
		
		/* remove node */
		removeNode(currNode);
		return;
	}
		
	/* function for processing removals */
	public void removeNode ( Node removalNode ) {
			if ( length == 1 ) /* if item is last item in list */
			{
				System.out.println("removing last item");
				head = tail = null;
			}
			else if ( removalNode == head ) /* if removalNode is the head of the list */
			{	System.out.println("removing head ");
				head = removalNode.getNext();
				
			}
			else if ( removalNode == tail ) /* if removalNode is the tail of the list */
			{
				removalNode.getPrev().setNext(null);
				tail = removalNode.getPrev();
				
			}
			else /* for all other nodes in the list */
			{
				removalNode.getPrev().setNext(removalNode.getNext());
				removalNode.getNext().setPrev(removalNode.getPrev());
			}
			
			/* decrement length count */
			length--;
			
		}
		
	public void print(PrintStream out) {
			
			Node currNode = head;
			
			while (currNode != null) 
			{
				
				out.printf("%s | %d\n", currNode.getValue(),currNode.getCount());
				currNode = currNode.getNext();
				// Implement me!
			} // end of print()
		}
		
	/*Node class*/
	private class Node {
		private String mValue;
		private Node mNext;
		private Node mPrev;
		int count;
		
		public Node(String value){
			mValue = value;
			mNext = null;
			mPrev = null;
			count = 1;
		}
		public void addCount() {
            this.count++;
        }

        public void minusCount() {
            this.count--;
        }

        public int getCount() {
            return count;
        }
		
		public String getValue(){
			return mValue;
		}
		public Node getNext() {
            return mNext;
        }
        
        public Node getPrev() {
            return mPrev;
        }

        public void setValue(String value) {
            mValue = value;
        }


        public void setNext(Node next) {
            mNext = next;
        }
        
        public void setPrev(Node prev) {
            mPrev = prev;
        }
	}
	
} // end of class LinkedListMultiset