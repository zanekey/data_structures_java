import java.io.PrintStream; 

public class SortedLinkedListMultiset<T> extends Multiset<T>
{
	/*Reference to the head of the list*/
	protected Node head;
	
	/*Reference to the tail of the list*/
	protected Node tail;
	
	/*The length of the list*/
	protected int length;
	
	/* Constructor */
	public SortedLinkedListMultiset() {
		head = null;
		tail = null;
		length = 0;	
	} // end of SortedLinkedListMultiset()
	
	/* add node to list */
	public void add(T item) {
		
		/* instantiate Nodes */
		Node currentNode = head;
		Node newNode = new Node((String) item);
		
		/* adding first Node to the list */
		if(head == null)
		{
			head = newNode;
			tail = newNode;
			length++;
			return;
		}
	
		/* traverse through list */
		while(currentNode != null)
		{
			/* check if node already exists in list */
			if (newNode.getValue().equals(currentNode.getValue())) 
			{
				currentNode.addCount();
				return;   
			}
			
			/* compare between current Nodes and new Node and allocate appropiate position for new Node */
			if(newNode.getValue().compareToIgnoreCase(currentNode.getValue()) <= 0)
			{	
				/* add new Node */
				addNode (currentNode , newNode);
				return;
			}
				
			currentNode = currentNode.getNext();
		}
		
		/* if new node is to be the tail of the List */
		addNode (currentNode , newNode);
	
	} // end of add()
	
	/* function to process adding a node and setting properties */
	public void addNode ( Node currentNode , Node newNode ) {
		if ( currentNode == head ) /* if currentNode is the head of the list */
		{
			currentNode.setPrev(newNode);
			newNode.setNext(currentNode);
			head = newNode;
		}
		else if ( currentNode == null ) /* if currentNode is the tail of the list */
		{
			tail.setNext(newNode);
			newNode.setPrev(tail);
			tail = newNode;
		}
		else /* for all other nodes in the list */
		{
			
			newNode.setNext(currentNode);
			newNode.setPrev(currentNode.getPrev());
			currentNode.setPrev(newNode);	
		}
		
		/* increment length */
		length++;
	}
	
	/* search list for specific item */
	public int search(T item) {
		
		/* traversal node */
		Node currentNode = head;
		
		/* traverse through list */
		while(currentNode != null)
		{
			/* if search element is found */
			if(currentNode.getValue().equals((String) item))
			{
				return currentNode.getCount();
			}
			
			//traverse to the next node
			currentNode = currentNode.getNext();
		}
		
		/* if no element is found return 0 */
		return 0;
	} // end of add()
	
	/* remove one instance of available item in list */
	public void removeOne(T item) {
		// Implement me!
		
		/* instantiate nodes */
		Node currentNode = head;
		Node removeNode = new Node((String)item);
		
		while (currentNode != null) /* traverse through entire list */
		{	
			/* check if item is available in list */
			if (currentNode.getValue().equals(removeNode.getValue())) 
			{
				if ( currentNode.getCount() > 1)/* if more than one item exists */
				{	
					/* decrement count */
					currentNode.count--;
					return;
				}
				else /* if item is last of its kind*/
				{
					/* remove item and rearrange node properties accordingly */
					removeNode ( currentNode );
					return;
				}	
            }
			
			// traverse to next node in list
			currentNode = currentNode.getNext();
		}
		
	} // end of removeOne()
	
	/*remove all instances of item in list */
	public void removeAll(T item) {
		
		/* instantiate nodes */
		Node currentNode = head;
		Node removeNode = new Node((String)item);
		
		/* traverse through entire list */
		while (currentNode != null)
		{	
			/* check if removal node is available in list */
			if (currentNode.getValue().equals(removeNode.getValue())) 
			{
				/* remove corresponding node */
				removeNode(currentNode);
			}
			// traverse to next node in list
			currentNode = currentNode.getNext();			
        }
	} // end of removeAll()
	
	/* function for processing removals */
	public void removeNode ( Node removalNode ) {
		if ( length == 1 ) /* if item is last item in list */
		{
			head = tail = null;
		}
		else if ( removalNode == head ) /* if removalNode is the head of the list */
		{
			head = removalNode.getNext();
			
		}
		else if ( removalNode == tail) /* if removalNode is the tail of the list */
		{
			tail = removalNode.getPrev();
		}
		else /* for all other nodes in the list */
		{
			removalNode.getPrev().setNext(removalNode.getNext());
			removalNode.getNext().setPrev(removalNode.getPrev());
		}
		
		/* decrement length count */
		length--;
		
	}
	
	/* print all items contained in list */
	public void print(PrintStream out) {
		
		/* instantiate node */
		Node currentNode = head;
	
		/* traverse nodes and print */
        while (currentNode != null) 
		{
			/* print current node and move onto next */
            out.printf("%s | %d\n", currentNode.getValue(),currentNode.getCount());
            currentNode = currentNode.getNext();
		}
		
	} // end of print()
	
	/*Node class*/
	private class Node {
		
		private String mValue;
		private Node mNext;
		private Node mPrev;
		int count;
		
		public Node(String value){
			mValue = value;
			mNext = null;
			mPrev = null;
			count = 1;
		}
		public void addCount() {
            count++;
        }

        public void minusCount() {
            count--;
        }

        public int getCount() {
            return count;
        }
		
		public String getValue(){
			return mValue;
		}
		public Node getNext() {
            return mNext;
        }
        
        public Node getPrev() {
            return mPrev;
        }

        public void setValue(String value) {
            mValue = value;
        }


        public void setNext(Node next) {
            mNext = next;
        }
        
        public void setPrev(Node prev) {
            mPrev = prev;
        }
	}

} // end of class SortedLinkedListMultiset